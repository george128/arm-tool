include Makefile.config

SRCDIR=src
BINDIR=bin
BUILDDIR=build
TOOLDIR=tools
DLDIR=dl

COMPILE_TRAGET=arm-none-eabi
CROSS_PREFIX=arm-none-eabi

gcc_all_arch=gcc-$(GCC_VER).tar.xz
gcc_arch=gcc-core-$(GCC_VER).tar.xz
gpp_arch=gcc-g++-$(GCC_VER).tar.xz

BINUTILS_ARCH=binutils-$(BINUTILS_VER).tar.gz

LIBC_ARCH=newlib-$(LIBC_VER).tar.gz

GDB_ARCH=gdb-$(GDB_VER).tar.xz

ARCH=$(shell uname -i)
COMPILE_ARGS=--enable-interwork --enable-multilib --enable-lto --with-system-zlib \
    --disable-nls --disable-libssp \
    --with-cpu=cortex-m4 --with-fpu=fpv4-sp-d16 --with-mode=thumb --with-float=hard
COMPILE_ARGS_GCC=$(COMPILE_ARGS)

TEST_CFLAGS=-Wall -Wextra -Warray-bounds -mlittle-endian \
    -mthumb -mcpu=cortex-m4 -mthumb-interwork \
    -mfloat-abi=hard -mfpu=fpv4-sp-d16

WGET_FLAGS=-4 -nd -c

all_tools: $(TOOLDIR) $(TOOLDIR)/bin/$(CROSS_PREFIX)-as \
    $(TOOLDIR)/bin/$(CROSS_PREFIX)-gcc \
    $(TOOLDIR)/$(CROSS_PREFIX)/lib/libc.a \
    $(TOOLDIR)/bin/$(CROSS_PREFIX)-g++ \
    $(TOOLDIR)/bin/$(CROSS_PREFIX)-gdb

$(BINDIR):
	mkdir -p $(BINDIR)
$(TOOLDIR):
	mkdir -p $(TOOLDIR)
$(BUILDDIR):
	mkdir -p $(BUILDDIR)
$(DLDIR):
	mkdir -p $(DLDIR)

$(DLDIR)/$(gcc_all_arch): $(DLDIR)
	wget $(WGET_FLAGS) -O $@ http://ftp.gnu.org/gnu/gcc/gcc-$(GCC_VER)/$(gcc_all_arch)

$(DLDIR)/$(gcc_arch): $(DLDIR)
	wget $(WGET_FLAGS) -O $@ http://ftp.gnu.org/gnu/gcc/gcc-$(GCC_VER)/$(gcc_arch)

$(DLDIR)/$(gpp_arch): $(DLDIR)
	wget $(WGET_FLAGS) -O $@ http://ftp.gnu.org/gnu/gcc/gcc-$(GCC_VER)/$(gpp_arch)

$(BUILDDIR)/gcc-src: $(BUILDDIR) $(DLDIR)/$(gcc_all_arch)
	tar xJf $(DLDIR)/$(gcc_all_arch) --keep-newer-files -C $(BUILDDIR)
#        patch --verbose -i $(PWD)/gcc_no_gcj.patch -d $(TOOLDIR)/gcc-$(GCC_VER)
	ln -sf gcc-$(GCC_VER) $@

$(TOOLDIR)/bin/$(CROSS_PREFIX)-gcc: $(TOOLDIR)/bin/$(CROSS_PREFIX)-as $(BUILDDIR)/gcc-src $(BUILDDIR)/libc-src
	mkdir -p $(BUILDDIR)/gcc-build
	echo $(CURDIR)
	(cd $(BUILDDIR)/gcc-build && PATH=$(CURDIR)/$(TOOLDIR)/bin:$(PATH) \
	    ../gcc-src/configure \
		--prefix=$(CURDIR)/$(TOOLDIR) \
		--target=$(COMPILE_TRAGET)  \
		--enable-languages=c,c++ \
		$(COMPILE_ARGS_GCC) \
		--with-newlib --with-headers=../libc-src/newlib/libc/include \
	    && make all-gcc install-gcc)

$(TOOLDIR)/bin/$(CROSS_PREFIX)-g++: $(TOOLDIR)/$(CROSS_PREFIX)/lib/libc.a $(BUILDDIR)/gcc-src
	(cd $(BUILDDIR)/gcc-build && PATH=$(CURDIR)/$(TOOLDIR)/bin:$(PATH) \
	    make all install)

$(DLDIR)/$(BINUTILS_ARCH): $(DLDIR)
	wget $(WGET_FLAGS) -O $@ http://ftp.gnu.org/gnu/binutils/$(BINUTILS_ARCH)

$(BUILDDIR)/binutils-src: $(BUILDDIR) $(DLDIR)/$(BINUTILS_ARCH)
	tar xzf $(DLDIR)/$(BINUTILS_ARCH) --keep-newer-files -C $(BUILDDIR)
	ln -sf binutils-$(BINUTILS_VER) $@

$(TOOLDIR)/bin/$(CROSS_PREFIX)-as: $(BUILDDIR)/binutils-src
	mkdir -p $(BUILDDIR)/binutils-build
	(cd $(BUILDDIR)/binutils-build && ../binutils-src/configure \
            --prefix=$(CURDIR)/$(TOOLDIR) \
            --target=$(COMPILE_TRAGET) $(COMPILE_ARGS) \
            && make all install)

$(DLDIR)/$(LIBC_ARCH): $(DLDIR)
	wget $(WGET_FLAGS) -O $@ ftp://sourceware.org/pub/newlib/$(LIBC_ARCH)

$(BUILDDIR)/libc-src: $(BUILDDIR) $(DLDIR)/$(LIBC_ARCH)
	tar xzf $(DLDIR)/$(LIBC_ARCH) --keep-newer-files -C $(BUILDDIR)
	ln -sf newlib-$(LIBC_VER) $@

$(TOOLDIR)/$(CROSS_PREFIX)/lib/libc.a: $(BUILDDIR)/libc-src
	mkdir -p $(BUILDDIR)/libc-build
	(cd $(BUILDDIR)/libc-build \
	&& PATH=$(CURDIR)/$(TOOLDIR)/bin:$(PATH) \
	$(PWD)/$</configure \
	    --prefix=$(CURDIR)/$(TOOLDIR) \
	    --target=$(COMPILE_TRAGET) \
	    $(COMPILE_ARGS) \
	    && PATH=$(CURDIR)/$(TOOLDIR)/bin:$(PATH) \
	    make all install)

$(DLDIR)/$(GDB_ARCH): $(DLDIR)
	wget $(WGET_FLAGS) -O $@ http://ftp.gnu.org/gnu/gdb/$(GDB_ARCH)

$(BUILDDIR)/gdb-src: $(TOOLDIR) $(DLDIR)/$(GDB_ARCH)
	tar xJf $(DLDIR)/$(GDB_ARCH) --keep-newer-files -C $(BUILDDIR)
	ln -sf gdb-$(GDB_VER) $@

$(TOOLDIR)/bin/$(CROSS_PREFIX)-gdb: $(BUILDDIR)/gdb-src
	mkdir -p $(BUILDDIR)/gdb-build
	(cd $(BUILDDIR)/gdb-build && PATH=$(CURDIR)/$(TOOLDIR)/bin:$(PATH) \
	    ../gdb-src/configure \
		--prefix=$(CURDIR)/$(TOOLDIR) \
		--target=$(COMPILE_TRAGET) \
	    && make all install)

clean:
	rm -rf $(TOOLDIR) $(BUILDDIR) test
test: test.c
	$(TOOLDIR)/bin/$(CROSS_PREFIX)-gcc $(TEST_CFLAGS) -g -O2 $@.c -o $@
